from bs4 import BeautifulSoup
import requests, re, sys

__BASE_URL__ = "http://books.toscrape.com/"

def get_pages(category):
    html = requests.get(__BASE_URL__).content
    soup = BeautifulSoup(html, 'html.parser')

    #Obtem o texto da tag side_categories
    categories = soup.find(class_='side_categories')
    categories_list = categories.find('a', text=re.compile(category))
    page_url =  __BASE_URL__+categories_list.get("href")
    url_category = page_url.replace("index.html", "")
    urls_list = [page_url]

    while page_url is not None:
        request = requests.get(page_url).content
        soup = BeautifulSoup(request, 'html.parser')
        try:
            # Obtem a proxima pagina da url
            next = soup.select_one(".next a")
            page_url = url_category + str(next.get('href'))
            urls_list.append(page_url)
        except:
            page_url = None

    return urls_list

if __name__ == '__main__':
    urls_pages = get_pages("Sequential Art")

    for page in urls_pages:
        request = requests.get(page).content
        soup = BeautifulSoup(request, 'html.parser')

        # Obtem o texto da tag side_categories
        articles_list = soup.select(".product_pod h3 a")
        prices_list = soup.select(".price_color")

        for article, price in zip(articles_list, prices_list):
            title = str(article.get("title"))
            price = price.text.strip()
            book_url = requests.compat.urljoin(page, article.get("href"))
            print("Título: " + title + "\nPreço: " + price + "\nURL: " + book_url + "\n")





